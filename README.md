# reactnative-interview

This is interview task.
Please create simple react native application, 

#### Functionality:
Show splash screen on the beginning, and after 2 seconds show login screen.

###### Application should login using credentials:
username: email@example.com
password: test1234

###### login link:
> http://188.166.25.105:2525/login

###### Application should send post request with following json:
```
{
"username" : "email@example.com",
"password" : "test1234"
}
```

Application should show following assets:
